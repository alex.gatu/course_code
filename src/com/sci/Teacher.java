package com.sci;

/**
 * Created by algatu on 10/08/2017.
 */
public class Teacher extends Person {

    private int teachedHours;
    private float salary = 0;

    private final float PERHOURPAY=10;

    public Teacher() {

    }

    public Teacher(int a, String n, String s, float h, float w, String addr, boolean m) {
        super(a,n,s,h,w,addr,m);
    }

    @Override
    public void abstractMethod() {

    }

    public void teach(int hours) {
        teachedHours += hours;
        salary = salary + hours * PERHOURPAY;
    }

    public void printTeacherStatus() {
        System.out.println(getName() + " teached  for " + teachedHours + " and his salary is now " + salary);
    }

    public int getTeachedHours() {
        return teachedHours;
    }

    public float getSalary() {
        return salary;
    }

}
