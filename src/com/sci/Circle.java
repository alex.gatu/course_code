package com.sci;

import java.awt.*;

/**
 * Created by algatu on 03/08/2017.
 */
public class Circle extends ShapeInheritance {
    double radius;

    void setRadius(double rad) {

        radius = rad;
    }

    @Override
    public double getArea() {
        if (radius!= 0) {
            return 3.14159264 * radius * radius;
        }
        return 0;
    }

    @Override
    public String toString() {
        return "Circle toString overriden method";
    }

    @Override
    public void draw() {
        System.out.println("Draw overwritten!");
    }

    public void ceva() {
        super.draw();
        draw();
    }

}
