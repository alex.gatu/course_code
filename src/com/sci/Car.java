package com.sci;

import java.awt.*;

/**
 * Created by algatu on 08/08/2017.
 */
public class Car {
    private float fuelLevel;
    private int gear;
    private float speed;
    private Color color;

    private int MAXGEAR; // nr viteze
    private float MAXFUELCAPAC; // litrii
    private float MAXSPEED; // max km/h

    public int publicAttribute;

    public Car() {
        /*MAXGEAR=6;
        MAXFUELCAPAC=60;
        MAXSPEED=200;
        this.color = Color.white;*/
        this(6,60,200,Color.white);
    }

    public Car(int gear, float fuelCapac, float maxSpeed, Color c) {
        MAXGEAR=gear;
        MAXFUELCAPAC=fuelCapac;
        MAXSPEED=maxSpeed;
        this.color = c;
    }

    public float getFuelLevel() {
        return this.fuelLevel;
    }

    public void setFuelLevel(float newLevel) {
        if (newLevel > MAXFUELCAPAC) {
            System.out.println("The max level cannot exceed the car capacity of " + MAXFUELCAPAC);
        }
        else {
            fuelLevel = newLevel;
            System.out.println("Fuel level is now " + fuelLevel);
        }
    }

    public int getGear() {
        return this.gear;
    }

    public void setGear(int newGear) {
        if (newGear > MAXGEAR) {
            System.out.println("The gear number must not be greater than the supported max gear of " + MAXGEAR);
        }
        else {
            gear = newGear;
        }
    }

    public float getSpeed() {
        return this.speed;
    }

    public void setSpeed(float newSpeed) {
        if (newSpeed > MAXSPEED) {
            System.out.println("The speed cannot exceed the max permitted speed of " + MAXSPEED);
        }
    }

    public Color getColor() {
        return this.color;
    }

    public void setColor(Color newColor) {
        color = newColor;
    }

    public void accelerate(float speedDelta) {
        speed += speedDelta;
        System.out.println("Current speed is " + speed);
    }

    public void deccelerate(float speedDelta) {
        if (speed - speedDelta >0 ){
            speed -= speedDelta;
        }
        System.out.println("Current speed is " + speed);
    }
    public void steer (int angle) {
        speed *= 0.9;
        gearDown();
    }

    public void gearUp() {
        if (gear < MAXGEAR) {
            gear++;
        }
        System.out.println("Current gear level is " + gear);
    }
    public void gearDown() {
        if (gear > 0) {
            gear--;
        }
        System.out.println("Current gear level is " + gear);
    }

    public void paintCar (Color newColor) {
        if (newColor!=null) {
            setColor(newColor);
            System.out.println("Color was changed to " + newColor.toString());
        }
    }

    public static void testStatic() {

    }

}
