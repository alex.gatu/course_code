package com.sci;

/**
 * Created by algatu on 03/08/2017.
 */
public class Square extends  ShapeInheritance{
    double squareSide;

    public Square() {
        squareSide = 1;
    }

    public Square(double s) {
        squareSide = s;
    }

    void setSide (double squareSide) {
        this.squareSide = squareSide;
    }

    @Override
    public double getArea() {
        return squareSide * squareSide;
    }

}
