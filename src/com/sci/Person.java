package com.sci;

/**
 * Created by algatu on 03/08/2017.
 */
public abstract class Person implements PersonInterface{
    // Defining Person class attributes
    private int age;
    private String name;
    private String sex;
    private float height;
    private float weight;
    private String address;
    private boolean maritalStatus;

    // Default constructor
    public Person() {

    }

    // Defining Person class methods
    // Specific constructor
    public Person (int a, String n, String s, float h, float w, String addr, boolean m) {
        age = a;
        name = n;
        sex = s;
        height = h;
        weight = w;
        address = addr;
        maritalStatus = m;
    }

    public abstract void abstractMethod();

    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    public float getWeight() {
        return weight;
    }


    public void eat(String type) {
        if (type=="lunch") {
            weight += 1;
        }
        else
            if (type=="dinner") {
                weight += 0.2;
            }
            else {
            weight += 0.5;
            }
    }

    @Override
    public void read(String book, int pageCount) {
        System.out.println("Reading " + pageCount + " pages from book " + book);
    }

    public void drink(String drinkType) {
        if (drinkType == "alchohol") {
            weight += 0.5;
        }
        else {
            weight+= 0.1;
        }
    }

    public void getMarried() {
        if (maritalStatus) {
            System.out.println("Already married");
        }
        else {
            maritalStatus = true;
            address = "Str ...";
        }
    }

    public void divorce() {
        maritalStatus = false;
        address = "Str ...";
    }

    public void exercise(int minutes) {
        weight -= 0.1 * minutes;
    }
}
