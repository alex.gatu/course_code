package com.sci;

import java.awt.*;

/**
 * Created by algatu on 10/08/2017.
 */
public class ShapeInheritance {

    private Color color;

    public ShapeInheritance() {
        color= Color.blue;
    }

    public double getArea() {
        return 0;
    }

    public ShapeInheritance(Color c) {
        color = c;
    }

    public void draw() {
        System.out.println("Drawing the shape with Color " + color.toString());
    }

    public void erase() {
        System.out.println("Erasing the shape with Color " + color.toString());
        color = null;
    }

    public void setColor(Color c) {
        color = c;
    }

    public Color getColor() {
        return color;
    }
}
