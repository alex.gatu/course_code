package com.sci;
import com.sci.Circle;

import java.awt.*;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;
import java.io.*;

public class Main {
    public enum logLevel {
        INFO,
        PRINT,
        ERROR
    }

    public enum StudentType {
        STUDENT,
        TEACHER
    }

    public static double circeArea(int r) throws Exception {
        if (r < 0) {
            throw new Exception("Radius of the circle must be positive!");
        }
        return Math.PI * r * r;
    }

    public static double circleLength(int r) throws MyPersonalException {
        if (r < 0) {
            throw  new MyPersonalException();
        }
        return Math.PI * r * 2;
    }

    public static int myParseInt(ArrayList<String> myString) throws MyPersonalException {
        int x = 0;
        try {
            for (String s : myString) {
                x = Integer.parseInt(s);
            }
            Integer.parseInt(myString.get(15));

        }
        catch (NumberFormatException ex) {
            System.out.println("Number introduced contains also non-numeric chars");

        }
        catch (IndexOutOfBoundsException ex1) {
            System.out.println("Array index out of bounds exception");
            throw new MyPersonalException("Index oob custom ex");
        }
        finally {
            System.out.println("I am executing no matter what !");
        }

        return x;

    }

    public static void main(String[] args) throws Exception {
        logLevel log = logLevel.INFO;
        /*System.out.println("Hello world!");

        Circle c1 = new Circle();
        Circle c2 = new Circle();
        c1.setRadius(2.0);
        c2.setRadius(3.0);
        System.out.println(c1.getArea() + " - " + c2.getArea());
        */
        /*Car c = new Car();
        c.publicAttribute=100;

        Light l1 = new Light();
        l1.turnOn();
        l1.turnOn();
        System.out.println(l1.getIntensity());
        l1.brighten(10);
        l1.getIntensity();
        System.out.println(l1.getIntensity());
        l1.brighten(100);
        System.out.println(l1.getIntensity());
        l1.dim(100);
        System.out.println(l1.getIntensity());
        l1.turnOff();

        Light l2 = new Light();

        Car c1 = new Car();
        c1.setColor(Color.blue);
        System.out.println(c1.getColor().toString());
        c1.paintCar(Color.green);
        System.out.println(c1.getColor().toString());
        c1.setFuelLevel(10);
        c1.setFuelLevel(1000);
        for (int i =0;i<5;i++) {
            c1.accelerate(10);
            System.out.println(c1.getSpeed());
        }

        Car c2 = new Car(5,50,150, Color.black);
        Car c3 = new Car(14,300, 180,Color.blue);
        c2.setFuelLevel(200);
        c3.setFuelLevel(200);


        System.out.println(StaticCar.PI);
        StaticCar.start();
        StaticCar.start();

        StaticCar.stop();
        StaticCar.stop();

        Triangle t2 = new Triangle(Color.black);
        System.out.println(t2.getColor().toString());
        Triangle t1 = new Triangle();
        System.out.println(t1.getColor().toString());
        Circle c1 = new Circle();
        c1.setRadius(2);
        System.out.println(c1.getArea());
        System.out.println(c1.toString());
        c1.draw();
        c1.ceva();


        Person p1 = new Person(20,"Gigi","male", (float)1.71 , 71, "Str...", false) ;
        Person p2 = new Person(30, "Florica", "male", (float)1.82, 80, "aaaa", true);
        Person p3 = new Person(28, "Andreea", "female", (float)1.59, 60, "aaaa", true);
        Teacher t1 = new Teacher(40,"Cupcea", "male", (float)1.60, 85, "adaa", false);
        Student s1 = new Student(19,"Vasile", "male", (float)1.73, 68,"Vaslui", false);
        System.out.println("His weight was at start " + t1.getWeight());
        for (int i = 0; i< 30; i++) {
            int hours = (int) (Math.random() * 100 % 24) + 1;
            t1.teach(hours);
            t1.drink("cola");
            t1.eat("shaowrma");
            t1.exercise((int) (Math.random() *100 % 60));
        }
        t1.printTeacherStatus();
        System.out.println("His weight is now " + t1.getWeight());

        s1.doHomework("h1");
        s1.doHomework("h2");

        s1.skipClass("java 3");
        s1.skipClass("selenium");

        System.out.println(s1.toString());


        Person p1 = new Teacher(20,"Gigi","male", (float)1.71 , 71, "Str...", false);
        Student s1 = new Student(20,"Gigi","male", (float)1.71 , 71, "Str...", false);
        for (int i = 0; i< 30; i++) {
            int hours = (int) (Math.random() * 100 % 24) + 1;
            s1.party(hours, 5);
        }

        System.out.println("Student " +s1.getName()+ " partied for " +  s1.getPartiedHours() + " hours ");
        */
       Person[] people = { new Teacher(20,"Gigi","male", (float)1.71 , 71, "Str...", false) ,
        new Teacher(30, "Florica", "male", (float)1.82, 80, "aaaa", true)};
       /*
       for (Person p : people) {
           System.out.println("NUME: " + p.getName() + " WEIGHT: " + p.getWeight());
       }

       System.out.println("Array has length : " + people.length);

       for (int i = 0; i< people.length ; i++) {
           System.out.println("["+i+"] NUME: " + people[i].getName() + " WEIGHT: " + people[i].getWeight());
       }
       */

        List<String> arr = new ArrayList<String>(10);
        List arr2 = new ArrayList(2);
        arr.add("String1");
        arr.add("String2");
        arr.add(0,"String0");
        /*
        for(String s : arr) {
            System.out.println(s.toString());
        }

        for (int i=0;i<arr.size();i++) {

            System.out.println(arr.get(i));
        }
        //arr.remove(0);
        //arr.remove("kjkkjhlj");
        Iterator it = arr.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }


        Map m = new HashMap();
        m.put("key","value");
        m.put(1,3);
        m.put(2,"ab");

        HashMap<Integer,String> m2 = new HashMap<Integer,String>();
        m2.put(1,"bbbb");
        for (int i = 0; i< 30; i++) {
            int key = (int) (Math.random() * 100 % 24) +1;
            m2.put(key,"String" + i);
            //System.out.println("Putting in key: " + key + " Value: "+"String" + i );
        }

        Iterator hashMapInterator = m2.keySet().iterator();
        while(hashMapInterator.hasNext()) {
            Integer key = (Integer) hashMapInterator.next();
            //System.out.println("For KEY: " + key + " the VALUE is: " + m2.get(key));
        }

        m2.remove(1);
        for (Integer key : m2.keySet()) {
            if (log==logLevel.PRINT) {
                System.out.println("For KEY: " + key + " the VALUE is: " + m2.get(key));
            }
        }

        Person t1 = new Teacher(40,"Cupcea", "male", (float)1.60, 85, "adaa", false);
        Person s1 = new Student(19,"Vasile", "male", (float)1.73, 68,"Vaslui", false);

        HashMap<StudentType,Person> map = new HashMap<>();
        map.put(StudentType.STUDENT,s1);
        map.put(StudentType.TEACHER,t1);

        /*Iterator it1 = map.keySet().iterator();
        while(it1.hasNext()) {
            Person x = map.get(it1.next());
            System.out.println(it1.next() + " : " + x.getName());
        }


        for (Object key : map.keySet()) {
            System.out.println(key + " : " + map.get(key).getName());
        }*/

        //throw new IllegalArgumentException("My friendly message");
       // System.out.println(myParseInt("1234a"));
        ArrayList<String> myArray = new ArrayList<>();
        for (int i =0;i<5; i++) {
            if (i==4) {
                myArray.add(i+"");
            }
            else {
                myArray.add(i+"");
            }
        }

        try {
            myParseInt(myArray);
        }
        catch (MyPersonalException ex) {
            for (StackTraceElement st : ex.getStackTrace()) {
                System.out.println(st.getLineNumber()+ " : " + st.toString());
            }
        }
        //System.out.println("adasdasdas");
        /*
        try {
            System.out.println(circleLength(-1));
        }
        catch (MyPersonalException ex) {
            System.out.println("Custom extended exception");
        }
        catch (Exception ex1) {
            System.out.println("Generic exception");
        }

        System.out.println(circleLength(-1));*/

    }



}
