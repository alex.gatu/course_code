package com.sci;

/**
 * Created by algatu on 08/08/2017.
 */
public class Light {

    private boolean state;
    private float currentIntesity;
    private final float MAXPOWER = 100; // Bec de 100W

    public boolean getState () {
        return this.state;
    }

    public void setState(boolean newState) {
        if (state == true && newState == true) {
            System.out.print("The state is already true (light is already on)");
        }
        else {
            if (state == false && newState == false) {
                System.out.print("The state is already false (light is already off)");
            }
            else {
                state = newState;
            }
        }
    }

    public float getIntensity() {
        return this.currentIntesity;
    }

    public void setIntensity(float newIntensity) {
        if (newIntensity > MAXPOWER) {
            System.out.println("New intensity must not exceed current max intesity of " + MAXPOWER);
        }
        else {
            currentIntesity = newIntensity;
        }
    }

    public void turnOn() {
        if (state) {
            System.out.println("The light is already ON");
        } else {
            state = true;
            currentIntesity = MAXPOWER * 10 / 100;
        }
    }

    public void turnOff() {
        if (!state)  {
            System.out.println("The light is already OFF");
        }
        else {
            state = false;
            currentIntesity = 0;
        }
    }

    public void dim(float intensity) {
        if (currentIntesity - intensity == 0) {
            if (currentIntesity > 0) {
                state = false;
                System.out.println("Light has been turned OFF!");
            }
            currentIntesity = 0;
        }
        if (currentIntesity - intensity < 0) {
            System.out.println("Cannot decrease light with more than " + currentIntesity);
        }
        if (currentIntesity - intensity > 0) {
            currentIntesity -= intensity;
        }
    }

    public void brighten(float intensity) {
        if (currentIntesity + intensity > MAXPOWER) {
            currentIntesity = MAXPOWER;
            System.out.println("The light reached its max power !!");
        }
        else
            if (currentIntesity + intensity > 0 ) {
                if (currentIntesity ==0) {
                    state = true;
                    System.out.println("Light has been turned ON!!");
                }
                currentIntesity += intensity;
            }
    }
}
