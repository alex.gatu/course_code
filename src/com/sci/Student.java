package com.sci;

/**
 * Created by algatu on 10/08/2017.
 */
public class Student extends Person implements StudentInterface {

    private String className = "";
    private String homeworks = "";

    private int partyHours = 0;
    private String examsTaken = "";

    public Student(int a, String n, String s, float h, float w, String addr, boolean m) {
        super(a,n,s,h,w,addr,m);
    }


    public Student() {

    }

    @Override
    public void abstractMethod() {
        System.out.println("Implementation of abstract methods");
    }

    public String toString() {
        return "Student " + super.getName() + " has skipped " + className + " and done homeworks: " + homeworks;
    }

    public void skipClass(String className) {
        System.out.println("Student skipped className " + className);
        this.className = this.className + ", " + className;
    }

    public void doHomework(String homew) {
        System.out.println("Student is doing homework " + homew);
        this.homeworks = this.homeworks + ", " + homew;
    }

    @Override
    public void party(int hours, int beerCount) {
        System.out.println("Student " + getName() + " partied for " + hours + "hours and drank " + beerCount + "beers");
        partyHours+= hours;
    }

    @Override
    public void takeExam(String exam) {
        examsTaken = examsTaken + " , " + exam;
    }

    @Override
    public int getPartiedHours() {
        return partyHours;
    }

    public String getClassSkipped() {
        return className;
    }

    public String getDoneHomeworks() {
        return homeworks;
    }

}
