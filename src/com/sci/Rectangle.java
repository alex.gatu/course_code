package com.sci;

/**
 * Created by algatu on 03/08/2017.
 */
public class Rectangle {
    private double l;
    private double w;

    public void setLen(double len) {
        l=len;
    }

    public void setWid(double wid) {
        w=wid;
    }

    public Rectangle(double len, double wid) {
        l=len;
        w=wid;
    }

    public Rectangle() {

    }

    public double getArea() {
        return w*l;
    }

    public double getPerimeter(){
        return 2 * (w +l);
    }

    public double getDiagonal() {
        return Math.sqrt(Math.pow(w,2)+Math.pow(l,2));
    }


}
