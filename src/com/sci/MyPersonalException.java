package com.sci;

/**
 * Created by algatu on 22/08/2017.
 */
public class MyPersonalException extends Exception {

    public MyPersonalException() {
        super();
        System.out.println("My custom message for my personal exception without arguments");
    }

    public MyPersonalException(String custom) {
        super(custom);
        System.out.println("My custom message for my personal exception with String arg");
    }

}
