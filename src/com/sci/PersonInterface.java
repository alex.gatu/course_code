package com.sci;

/**
 * Created by algatu on 10/08/2017.
 */
public interface PersonInterface {

    String name = "aaa";

    // getter for the interface
    String getName();

    // setter for the interface
    void setName(String name);

    void eat(String food);

    void read(String book, int pageCount);

}
