package com.sci;

/**
 * Created by algatu on 10/08/2017.
 */
public interface StudentInterface {

    void doHomework(String homew);

    void party(int hours, int beerCount);

    int getPartiedHours();

    void takeExam(String exam);

}
