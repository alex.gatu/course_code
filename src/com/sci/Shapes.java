package com.sci;
import com.sci.*;

/**
 * Created by algatu on 03/08/2017.
 */
public class Shapes {
    public static void main(String[] args) {
        for (String s : args) {
            Circle c1 = new Circle();
            Square sq = new Square();
            Square sq2 = new Square(2.0);
            //System.out.println(sq2.getArea());
            double rad = Double.parseDouble(s);
            c1.setRadius(rad);
            sq.setSide(rad);
            System.out.println("Circle area: " + c1.getArea() + " Square area: " + sq.getArea());
        }
        Rectangle r1 = new Rectangle();
        Rectangle r2 = new Rectangle(2,4);
        System.out.println("Area is "+ r2.getArea() + "\nDiagonal is " + r2.getDiagonal() + "\nPerimeter is "+ r2.getPerimeter());
    }
}
