package com.sci;

/**
 * Created by algatu on 08/08/2017.
 */
public class StaticCar {

    private static boolean startStatus;

    public static float PI = (float) 3.1415;

    public static void start() {
        if (startStatus) {
            System.out.println("Car is already started !!");
        }
        else {
            startStatus = true;
            System.out.println("Car has been started!");
        }
    }

    public static void stop() {
        if (!startStatus) {
            System.out.println("The car is already stopped !!");
        }
        else {
            startStatus = false;
            System.out.println("Car has been stopped!");
        }
    }

}
