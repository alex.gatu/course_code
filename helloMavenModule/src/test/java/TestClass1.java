import org.junit.*;
import org.junit.Test;

/**
 * Created by algatu on 24/08/2017.
 */

public class TestClass1 {

    Circle c1 = new Circle();

    @BeforeClass
    public static void beforeClassMethod() {
        System.out.println("before class is executed");

    }

    @Before
    public void beoforeMethod() {
        System.out.println("simple before method was called!");
    }

    @Test
    public void test1() {
        System.out.println("Test 1 is here !");
        c1.setRadius(3);
        double result = c1.getArea();
        Assert.assertEquals(28.2735,result,0.01);

    }

    @Test
    public void test2() {
        c1.setRadius(7.5);
        double result = c1.getDiameter();
        Assert.assertEquals(15,result,0);
    }

    @After
    public void afterMethod() {
        System.out.println("After method is executed!");
    }

    @AfterClass
    public static void afterClassMethod() {
        System.out.println("After class is here!");
    }

}
